class voting_instance():
    voters = []
    items_to_vote_on = []
    still_voting = True
    winner = 0
    tie = False
    tied_item = 0

    def add_voter(self):
        voter_name = get_voter_name()
        new_voter = voter(voter_name, 6)
        self.voters.append(new_voter)

    def get_voter_list(self):
        all_voters = ""
        for voter in self.voters:
            all_voters += (voter.voter_name + ", ")
        all_voters = self.remove_unnecessary_comma(all_voters)
        return all_voters

    def print_voters(self):
        return self.get_voter_list()

    def add_item_to_vote_on(self):
        item_name = get_item_name()
        new_item = item_to_vote_on(item_name)
        self.items_to_vote_on.append(new_item)

    def get_item_list(self):
        all_items = ''
        item_number = 1
        for item in self.items_to_vote_on:
            all_items += (str(item_number) + ". " + item.item_name + "\n")
            item_number += 1
        return all_items

    def print_items(self):
        return self.get_item_list()

    def remove_unnecessary_comma(self, string_with_extra_comma):
        return string_with_extra_comma[:-2]

    def initiate_voting(self):
        while self.still_voting:
            self.print_voting_prompt(self.get_current_voter())

    def get_current_voter(self):
        for voter in self.voters:
            if voter.number_of_votes_remaining > 0:
                return self.voters.index(voter)
        self.still_voting = False

    def print_voting_prompt(self, current_voter):
        if self.still_voting == True:
            print("\n\n\n\n" + self.voters[current_voter].voter_name + ", you have " + str(
                self.voters[current_voter].number_of_votes_remaining) + " votes remaining.")
            print("Your first choice will use 3 votes, second will use 2 votes, and your third choice will use 1 vote.")
            print("Which item would you like to vote on?")
            self.get_user_vote(current_voter)

    def get_user_vote(self, current_voter):
        user_vote = int(input(self.get_items_not_voted_on(current_voter)))
        user_vote -= 1
        self.submit_vote(current_voter, int(user_vote))

    def get_items_not_voted_on(self, voter):
        items_not_voted_on = ''
        item_number = 1
        for item in self.items_to_vote_on:
            if (item_number not in self.voters[voter].items_voted_on):
                items_not_voted_on += (str(item_number) + ". " + item.item_name + "\n")
            item_number += 1
        return items_not_voted_on

    def submit_vote(self, current_voter, user_vote):
        if self.voters[current_voter].number_of_votes_remaining == 6:
            self.items_to_vote_on[user_vote].number_of_votes += 3
            self.voters[current_voter].number_of_votes_remaining -= 3
            self.voters[current_voter].items_voted_on.append(user_vote + 1)
        elif self.voters[current_voter].number_of_votes_remaining == 3:
            self.items_to_vote_on[user_vote].number_of_votes += 2
            self.voters[current_voter].number_of_votes_remaining -= 2
            self.voters[current_voter].items_voted_on.append(user_vote + 1)
        elif self.voters[current_voter].number_of_votes_remaining == 1:
            self.items_to_vote_on[user_vote].number_of_votes += 1
            self.voters[current_voter].number_of_votes_remaining -= 1
            self.voters[current_voter].items_voted_on.append(user_vote + 1)

    def determine_winner(self):
        max_number_of_votes = 0
        for item in self.items_to_vote_on:
            if item.number_of_votes > max_number_of_votes:
                self.winner = item
                max_number_of_votes = item.number_of_votes
                self.tie = False
            elif item.number_of_votes == max_number_of_votes:
                self.tie = True
                self.tied_item = item

    def print_winner(self):
        if self.tie == False:
            print("\n\n\n\n\n\n\n\n\n\n\n\n\n**************************************************\n\n"
                  "AND THE WINNER IS......\n\n"
                  + self.winner.item_name + " with " + str(self.winner.number_of_votes) + " votes!!!!!!\n"
                                                                                          "\n**************************************************\n")
        elif self.tie == True:
            print("\nThere was a tie...\n")
            print(self.winner.item_name + " with " + str(self.winner.number_of_votes) + " votes\n")
            print(self.tied_item.item_name + " with " + str(self.winner.number_of_votes) + " votes\n")

    def ask_to_view_extended_results(self):
        if input("Would you like to view the extended results?\n"
                 "(y)es\n"
                 "(n)o\n") == "y":
            print("\n")
            self.print_results()
        else:
            print("\nHave fun!!\n")

    def print_results(self):
        for voter in self.voters:
            voter_votes = voter.voter_name + " voted on (in order): "
            for item in voter.items_voted_on:
                voter_votes += (self.items_to_vote_on[(item - 1)].item_name + ", ")
            print(self.remove_unnecessary_comma(voter_votes))

        for item in self.items_to_vote_on:
            item_votes = str(self.items_to_vote_on.index(item) + 1) + ". " + item.item_name + " had " + str(
                item.number_of_votes) + " votes"
            print(item_votes)


class voter():
    number_of_votes_remaining = 0
    voter_name = ''
    items_voted_on = []

    def __init__(self, voter_name, number_of_votes_remaining):
        self.voter_name = voter_name
        self.number_of_votes_remaining = number_of_votes_remaining
        self.items_voted_on = []


class item_to_vote_on():
    number_of_votes = 0
    item_name = 0

    def __init__(self, item_name):
        self.item_name = item_name


def get_item_name():
    return input("Input a name for the item to vote on:\n")


def get_voter_name():
    return input("Input a name for the voter:\n")


def create_voting_instance():
    return voting_instance()


def ask_if_add_voter(new_voting_instance):
    return input("\nWould you like to add a voter?\n"
                 "Current voters are: \n" + new_voting_instance.print_voters() + "\n"
                                                                                 "\n(y)es\n"
                                                                                 "(n)o\n")


def ask_if_add_item(new_voting_instance):
    return input("\nWould you like to add an item to vote on?\n"
                 "Current items are: \n" + new_voting_instance.print_items() +
                 "\n(y)es\n"
                 "(n)o\n")


def add_voters(new_voting_instance):
    new_voting_instance.add_voter()
    add_another_voter = "y"
    while (add_another_voter == "y"):
        add_another_voter = ask_if_add_voter(new_voting_instance)
        if (add_another_voter == "y"):
            new_voting_instance.add_voter()


def add_items_to_vote_on(new_voting_instance):
    new_voting_instance.add_item_to_vote_on()
    add_another_item = "y"
    while (add_another_item == "y"):
        add_another_item = ask_if_add_item(new_voting_instance)
        if (add_another_item == "y"):
            new_voting_instance.add_item_to_vote_on()


def main():
    new_voting_instance = create_voting_instance()

    add_voters(new_voting_instance)
    add_items_to_vote_on(new_voting_instance)

    new_voting_instance.initiate_voting()
    new_voting_instance.determine_winner()
    new_voting_instance.print_winner()
    new_voting_instance.ask_to_view_extended_results()


main()
